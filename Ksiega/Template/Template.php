<?php

namespace Ksiega\Template;

class Template {
    
    static public function render($f,$d) {
        foreach ((array)$d as $k => $v) $$k = $v;
        ob_start();
        include('page'. DIRECTORY_SEPARATOR . $f . '.php');
        return ob_get_clean();
    }
}