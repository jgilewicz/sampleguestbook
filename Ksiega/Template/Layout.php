<?php

namespace Ksiega\Template;

class Layout {
    
    public function build ($data) {
        echo
        Template::render('layout',
                array(
                    'content' => Template::render($data['content']['file'], $data['content']['data']),
                    'menu' => Template::render('menu', null),
                ));
    }
}