<?php

namespace Ksiega\Controller;

use Ksiega\Model\GuestBookModel;
use Ksiega\Template;

class AdminController extends AbstractController {
    
    public function edit() {
        
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') return $this->editForm();
        
        $this->form = new AdminForm();
        $this->form->process();
        if ($this->form->error != null) 
            return $this->editForm();
        
        $model = new GuestBookModel();
        $model->edit($this->form);
        
        return $this->editForm();
    }
    
    public function editForm() {
        
        $model = new GuestBookModel();

        $view = new Template\Layout();
        $view->build(
                array(
                    'content' => array(
                        'file' => 'edit',
                        'data' => array(
                            'entries' => $model->all(),
                            'form' => $this->form,
                            ),
                    )   
                )
        );
    }
}