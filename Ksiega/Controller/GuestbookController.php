<?php

namespace Ksiega\Controller;

use Ksiega\Model\GuestBookModel;
use Ksiega\Template;

class GuestbookController extends AbstractController {
    
    public function listing() {
        $model = new GuestBookModel();
        $view = new Template\Layout();
        $view->build(
                array(
                    'content' => array(
                        'file' => 'listing',
                        'data' => array('entries' => $model->all()),
                    )   
                )
        );
    }
    
    public function wizard() {
        
        $this->form = new WizardForm();
        $this->form->process();
        if ($_SERVER['REQUEST_METHOD'] !== 'POST') 
            return $this->getWizardLayout($this->form->input['step']['value']);
        
        $model = new GuestBookModel();
        
        if ($this->form->input['step']['value'] === 'stepthree' && $this->form->error === null) {
            $model->add($this->form);
            return $this->listing();
        }
        
        if ($this->form->error === null) {
            $step_numbers = array_flip(WizardForm::$steps);
            $step_number = $step_numbers[substr($this->form->input['step']['value'], 4)] - 1;
            if ($step_number < 0) $step_number = 0;
            $this->getWizardLayout('step' . WizardForm::$steps[$step_number]);
        }
        
        return $this->getWizardLayout($this->form->input['step']['value']);
    }
    
    protected function getWizardLayout($name) {
        
        $view = new Template\Layout();
        $view->build(
                array(
                    'content' => array(
                        'file' => $name,
                        'data' => array('form' => $this->form),
                    )   
                )
        );
    }
}