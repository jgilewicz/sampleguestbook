<?php

namespace Ksiega\Controller;

abstract class AbstractForm {
    
    const PREG_EMAIL = '/^\(w+\.)+\w+@(\w+\.)+\w+$/';
    const PREG_ID = '/^\d+$/';
    const PREG_TEXT = '/^[\p{L} \._-]{2,500}$/u';
    const PREG_DATE = '/^(19|20)\d{2}-(0[1-9]|1[0-2])-([0-2][0-9]|3[0-1])$/';
    
    public $error = null;
    
    public $input = null;
    
    public function process() {
        foreach ($this->input as $name => $field) 
            if (!preg_match($field['pattern'], $field['value'])) {
                $this->error = $name;
                return $this;
            }
    }
    
    protected function addField($name, $field) {
        $this->input[$name] = $field;
    }
}
