<?php

namespace Ksiega\Controller;

class AdminForm extends AbstractForm {
    
    public function __construct() {
        $this->addField('id', array(
            'pattern' => self::PREG_ID,
            'value' => isset($_POST['id'])?$_POST['id']:null,
            'message' => 'nie ma takiego wpisu'
            )
        );
        $this->addField('comment', array(
            'pattern' => self::PREG_TEXT,
            'value' => isset($_POST['comment'])?$_POST['comment']:null,
            'message' => 'wpisz od 2 do 500 znaków'
            )
        );
    }
}