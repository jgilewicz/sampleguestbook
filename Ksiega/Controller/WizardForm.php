<?php

namespace Ksiega\Controller;

class WizardForm extends AbstractForm {
    
    public static $steps = array('one', 'two', 'three', 'submit');

    public function __construct() {
        
        $this->PREG_STEP = '/^(step(' . implode(self::$steps, '|') . ')|)$/';
        
        if (!isset($_SESSION['post'])) $_SESSION['post'] = array();
        
        $_SESSION['post'] = array_merge($_SESSION['post'], $_POST);
        $this->addField('step', array(
            'pattern' => $this->PREG_STEP,
            'value' => isset($_GET['step'])?$_GET['step']:'stepone',
            )
        );
        if ($this->input['step']['value'] == 'stepone')
            $this->addField('imie_nazwisko', array(
                'pattern' => self::PREG_TEXT,
                'value' => isset($_SESSION['post']['imie_nazwisko'])?$_SESSION['post']['imie_nazwisko']:null,
                'message' => 'Popraw Imię i Nazwisko',
                )
            );
        if ($this->input['step']['value'] == 'steptwo')
            $this->addField('birthdate', array(
                'pattern' => self::PREG_DATE,
                'value' => (isset($_SESSION['post']['year']) && isset($_SESSION['post']['month']) && isset($_SESSION['post']['day']))
                ?$_SESSION['post']['year'] . '-' . $_SESSION['post']['month'] . '-' . $_SESSION['post']['day']:null,
                'message' => 'Popraw datę urodzenia',
                )
            );
    }
}