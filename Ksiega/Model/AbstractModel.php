<?php

namespace Ksiega\Model;

abstract class AbstractModel {
    
    public function __construct() {
        
        $this->conn = new \mysqli('localhost', 'root', '54321', 'ksiega');
        $this->conn->set_charset ('utf8');
    }
    
    protected function runSQL ($q) {
        
        if (($r = $this->conn->query($q)) === false) 
            echo $this->conn->error . '<br />' . $q;
        return $r;
}

    protected function select($q) {
        
        $r = $this->runSQL($q);
        while($row = $r->fetch_assoc()) $rows[] = $row;
        return $rows;
    }
}