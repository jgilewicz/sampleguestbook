<?php

namespace Ksiega\Model;

class GuestBookModel extends AbstractModel {
    
    public function all() {
        return $this->select('select * from entries');
    }
    
    public function edit($form) {
        return $this->runSQL("update entries set comment = '{$form->input['comment']['value']}' where id = {$form->input['id']['value']}");
    }
    
    public function add($form) {
        return $this->runSQL("insert into entries (imie_nazwisko, city, birthdate) values "
            . "({$form->input['imie_nazwisko']['value']}, {$form->input['city']['value']}, {$form->input['imie_nazwisko']['birthdate']})");
    }
}