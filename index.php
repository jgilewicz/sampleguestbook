<?php

require_once 'autoload.php';
session_start();

use Ksiega\Controller\GuestbookController;

$c = new GuestbookController();
$a = isset($_GET['a'])?$_GET['a']:null;
$a = (preg_match('/^\w+$/', $a))?$a:'listing';
if (method_exists($c, $a) && is_callable(array($c, $a))) 
    call_user_func(array($c, $a));
