<?php

require_once 'autoload.php';

use Ksiega\Controller\AdminController;

$c = new AdminController();
$a = isset($_GET['a'])?$_GET['a']:null;
$a = (preg_match('/^\w+$/', $a))?$a:'editForm';
if (method_exists($c, $a) && is_callable(array($c, $a))) 
    call_user_func(array($c, $a));
